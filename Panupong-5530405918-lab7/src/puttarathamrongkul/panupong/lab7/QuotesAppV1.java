package puttarathamrongkul.panupong.lab7;
/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab6
 * 1.To learn how to use layout managers
 * 2.To learn how to create more GUI components which includes menus
 */


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class QuotesAppV1 extends JFrame {
	
	/**
	 * to generate serial version UID
	 */
	private static final long serialVersionUID = 1984775500408099398L;
	
	//to defined instance variable including JFrame call frame and JPanel call content
	JFrame frame;
	//to create constructor call QuotesAppV1
	public QuotesAppV1(String appname) {
		//to create new JFrame and JPanel
		frame = new JFrame(appname);
		JPanel content = new JPanel();
		//to use imageIcon and add that to content
		ImageIcon imageTime = new ImageIcon(this.getClass().getResource("/images/time.jpeg"));
		JLabel toLabel = new JLabel(imageTime);
		content.add(toLabel);
		//to add content to frame and set frame
		frame.setContentPane(content);
		frame.setSize(500, 650);
		frame.setLocation(100, 100);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				QuotesAppV1 qa = new
						QuotesAppV1("Quotes App V1");
				qa.addMenus();
			}
		});
	}

	public void addMenus() {
		//to add MenuBar
		JMenuBar menuBar = new JMenuBar();
		//to create JMenu call File and Edit
		JMenu fMen = new JMenu("File");
		JMenu edMen = new JMenu("Edit");
		//to add JMenuItems in JMenu call File
		fMen.add(new JMenuItem("New"));
		fMen.add(new JMenuItem("Open", new ImageIcon(this.getClass().getResource("/images/open-icon.png"))));
		fMen.add(new JMenuItem("Save"));
		fMen.add(new JMenuItem("Exit"));
		//to create JMenu call Set Color which contain JMenutems
		JMenu setColor = new JMenu("Set Color");
		setColor.add(new JMenuItem("Red"));
		setColor.add(new JMenuItem("Green"));
		setColor.add(new JMenuItem("Blue"));
		//to create JMenu call Set Font which contain sub JMenu call Size and Style
		JMenu setSize = new JMenu("Set Font");
		JMenu sizeSet = new JMenu("Size");
		//to add JMenuItems about size in sub JMenu call Size
		sizeSet.add(new JMenuItem("16"));
		sizeSet.add(new JMenuItem("18"));
		sizeSet.add(new JMenuItem("20"));
		//to create JMenu call Set Size which contain sub JMenu call Style
		JMenu styleSet = new JMenu("Style");
		//to add JMenuItems about style in sub JMenu call Style
		styleSet.add(new JMenuItem("Bold"));
		styleSet.add(new JMenuItem("Italic"));
		styleSet.add(new JMenuItem("Bold Italic"));
		//to add JMenu call Size and Style to Set Font's JMenu
		setSize.add(sizeSet);
		setSize.add(styleSet);
		//to add JMenu Set Color and Set Font to Edit menu
		edMen.add(setColor);
		edMen.add(setSize);
		//to add JMenu call File and Edit on MenuBar
		menuBar.add(fMen);
		menuBar.add(edMen);
		//to add JMenuBar to frame
		frame.setJMenuBar(menuBar);
	}
}
