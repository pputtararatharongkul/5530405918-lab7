package puttarathamrongkul.panupong.lab7;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * Create by Panupong Puttarathamrongkul 5530405918 lab6 1.To learn how to use
 * layout managers 2.To learn how to create more GUI components which includes
 * menus
 */

public class QuotesAppV2 extends QuotesAppV1 {
	
	JRadioButton th, en;
	JCheckBox wis, tech, soc, pol;
	public QuotesAppV2(String appname) {
		super(appname);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -2862252080620863991L;

	public static void main(String[] args) {

		SwingUtilities.invokeLater(new Runnable() {

			public void run() {
				QuotesAppV2 qa = new QuotesAppV2("Quotes App V2");
				qa.addComponents();
				qa.addMenus();
			}
		});
	}

	protected void addComponents() {
		JPanel content2 = new JPanel();
		content2.setLayout(new BorderLayout());
		JPanel contentTop = new JPanel();
		
		contentTop.setLayout(new BorderLayout());
		JLabel curQ = new JLabel("Current Quote");
		JTextArea quoteArea = new JTextArea(setText() , 10, 15);
		quoteArea.setFont(new Font("SansSerif", Font.BOLD, 16));
		quoteArea.setForeground(Color.BLUE);
		quoteArea.setLineWrap(true);
		contentTop.add(curQ, BorderLayout.NORTH);
		contentTop.add(quoteArea, BorderLayout.CENTER);
		
		JPanel contentMiddle = new JPanel(new GridLayout(4,2));
		
		JPanel midFstBox = new JPanel(new BorderLayout());
		JPanel middleFstLine = new JPanel(new GridLayout(1,2));
		JLabel leftLanguage = new JLabel("Language:");
		th = new JRadioButton("Thai");
		en = new JRadioButton("English");
		JPanel rightJRadio = new JPanel(new FlowLayout());
		rightJRadio.add(th);
		rightJRadio.add(en);
		middleFstLine.add(leftLanguage, BorderLayout.WEST);
		middleFstLine.add(rightJRadio, BorderLayout.EAST);
		midFstBox.add(middleFstLine);
		
		JPanel midSecBox = new JPanel(new GridLayout(1,1));
		JPanel middleSecLine = new JPanel(new GridLayout(1,2));
		JLabel lefTypes = new JLabel("Types:");
		JPanel rightJCheck = new JPanel(new FlowLayout());
		wis = new JCheckBox("Wisdom");
		tech = new JCheckBox("Technology");
		soc = new JCheckBox("Society");
		pol = new JCheckBox("Politics");
		rightJCheck.add(wis);
		rightJCheck.add(tech);
		rightJCheck.add(soc);
		rightJCheck.add(pol);
		midSecBox.add(middleSecLine);
		middleSecLine.add(lefTypes, BorderLayout.WEST);
		middleSecLine.add(rightJCheck, BorderLayout.EAST);
		
		JPanel midThrdBox = new JPanel(new GridLayout(1,2));
		JPanel middleThrdLine = new JPanel(new BorderLayout());
		JLabel numQ = new JLabel("Number of quotes:");
		String numQList[] = {"1", "2", "3"};
		JPanel rightCombo = new JPanel();
		JComboBox jcomboQ = new JComboBox(numQList);
		rightCombo.add(jcomboQ);
		middleThrdLine.add(numQ, BorderLayout.WEST);
		middleThrdLine.add(rightCombo, BorderLayout.EAST);
		midThrdBox.add(middleThrdLine);
		
		JPanel midFothBox = new JPanel(new GridLayout(1,2));
		JPanel middleFothLine = new JPanel(new GridLayout());
		JPanel rightList = new JPanel();
		JLabel authors = new JLabel("Authors:");
		String aurList[] = {"Achara Klinsuwan", "Buddha", "Dungtrin",
				"Earl Nightingale", "Einstein", "Ghandi", "Phra Paisal Visalo",
				"V.Vajiramedhi"};
		JList jlistQ = new JList(aurList);
		JScrollPane scrollPanel = new JScrollPane(jlistQ);
		rightList.add(scrollPanel);
		middleFothLine.add(authors, BorderLayout.WEST);
		middleFothLine.add(rightList, BorderLayout.EAST);
		midFothBox.add(middleFothLine);
		
		contentMiddle.add(midFstBox);
		contentMiddle.add(midSecBox);
		contentMiddle.add(midThrdBox);
		contentMiddle.add(midFothBox);
		
		JPanel contentBottom = new JPanel(new BorderLayout());
		JPanel buttonPane = new JPanel();
		buttonPane.add(new JButton("Submit"));
		buttonPane.add(new JButton("Cancel"));
		contentBottom.add(buttonPane);
		
		content2.add(contentTop, BorderLayout.NORTH);
		content2.add(contentMiddle, BorderLayout.CENTER);
		content2.add(contentBottom, BorderLayout.SOUTH);
		frame.setContentPane(content2);
		

	}

	public String setText() {
		String text = "��������Ҿ�ǧ�� ʹյ��ҹ����� ���Ш����ҧ �ѹ���ҹ����� �Ѻ��ͧ����� ͹Ҥ������֧�ѡ��     "+
				"�֧��������������繻Ѩ�غѹ ��觷���Դ��͹��������� ��觷��Դ��Ҩ��Դ��� ��ǹ�����Ҿ�ǧ��"+
				"������Ƿ���դ������� ������Ƿ���繨�ԧ ��ͻѨ�غѹ�͹����ͧ�\n"+"\n"+
				"��������ͷء��觷�����Թ �ҡ���Թ����ͧ���ҧ����ͧ�ͧ����� ���Թ��仡Ѻ�� "+
				"���ҡ���Թ�ҧ����ͧ������� �ͧ����� ����º������ �ѧࡵ��ó� ���¹��������ԧ���µ���ͧ "+
				"���ٴ�֧�����蹷�����������˹�� �������͹��� �ǡ�ҡ��ѧ��觿ѧ�������ͧ���ǡѺ������� "+
				"���������·�ȹ� ����þԨ�ó�����ͨС���Ƕ֧�ؤ�ŷ�����\n"+"\n"+
				"-Achara Klinsuwan�";
		return text;
	}
}
